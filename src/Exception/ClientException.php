<?php


namespace Tongshangyun\Client\Exception;


class ClientException extends TongshangyunException
{
    /**
     * @param $errorMessage
     * @param $errorCode
     * @param null $previous
     */
    public function __construct($errorMessage, $errorCode, $previous = null)
    {
        parent::__construct($errorMessage, 0, $previous);
        $this->errorMessage = $errorMessage;
        $this->errorCode    = $errorCode;
    }

}