<?php

namespace Tongshangyun\Client\Support;

use Tongshangyun\Client\Exception\ClientException;
use Tongshangyun\Client\Filter\ClientFilter;

class Sign
{
    public static function string($ssoId, $strReq, $timestamp, $path, $pwd)
    {
        $dataStr = $ssoId . $strReq . $timestamp;
        $text = base64_encode(hash('md5', $dataStr, true));
        $privateKey = self::loadPrivateKey($path, $pwd);
        openssl_sign($text, $sign, $privateKey);
        openssl_free_key(openssl_pkey_get_private($privateKey)); //释放的时候提示没有这个函数
        $sign = base64_encode($sign);
        return $sign;
    }

    /**
     * 从证书文件中装入私钥 pem格式;
     */
    private static function loadPrivateKey($path, $pwd)
    {
        $str = explode('.', $path);
        $suffix = $str[count($str) - 1];
        if ($suffix == "pfx") {
            return self::loadPrivateKeyByPfx($path, $pwd);
            $priKey = file_get_contents($path);
            $res = openssl_get_privatekey($priKey, $pwd);
            return ClientFilter::priKey($res);
        }
        if ($suffix == "pem") {
            $priKey = file_get_contents($path);
            $res = openssl_get_privatekey($priKey, $pwd);
            return ClientFilter::priKey($res);
        }
    }

    /**
     * 从证书文件中装入私钥 Pfx 文件格式
     */
    private static function loadPrivateKeyByPfx($path, $pwd)
    {
        if (file_exists($path)) {
            $priKey = file_get_contents($path);
            if (openssl_pkcs12_read($priKey, $certs, $pwd)) {
                $privateKey = $certs['pkey'];
                return $privateKey;
            }
        }
        ClientFilter::isPathFile();
    }

}
