<?php

namespace Tongshangyun\Client\Traits;

use Tongshangyun\Client\Filter\ClientFilter;

/**
 * 入口门
 * Trait AcsTrait
 */
trait AcsTrait
{
    /**
     * @var string
     */
    public $sysId;

    /**
     * @var string
     */
    public $timestamp;

    /**
     * @var string
     */
    public $sign;

    /**
     * @var string
     */
    public $version;

    /**
     * @var string
     */
    public $service;

    /**
     * @var string
     */
    public $method;

    /**
     * @param $sysId
     * @return $this
     * @throws \tongshangyun\client\Exception\ClientException
     */
    public function sysId($sysId)
    {
        $this->sysId = ClientFilter::sysId($sysId);

        return $this;
    }

    /**
     * @param $version
     * @return $this
     * @throws \tongshangyun\client\Exception\ClientException
     */
    public function version($version)
    {
        $this->version = ClientFilter::version($version);

        return $this;
    }

    /**
     * @param $service
     * @return $this
     * @throws \tongshangyun\client\Exception\ClientException
     */
    public function service($service)
    {
        $this->service = ClientFilter::service($service);

        return $this;
    }

    /**
     * @param $method
     * @return $this
     * @throws \tongshangyun\client\Exception\ClientException
     */
    public function method($method)
    {
        $this->method = ClientFilter::method($method);

        return $this;
    }

}