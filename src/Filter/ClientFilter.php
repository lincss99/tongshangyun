<?php

namespace Tongshangyun\Client\Filter;

use Tongshangyun\Client\Constant;
use Tongshangyun\Client\Exception\ClientException;

class ClientFilter
{
    /**
     * @param $sysId
     * @return string
     * @throws ClientException
     */
    public static function sysId($sysId)
    {
        if (!is_string($sysId)) {
            throw new ClientException(
                'sysId must be a string',
                Constant::INVALID_ARGUMENT
            );
        }

        if ($sysId === '') {
            throw new ClientException(
                'sysId cannot be empty',
                Constant::INVALID_ARGUMENT
            );
        }

        if (!preg_match("/^[0-9]+$/", $sysId)) {
            throw new ClientException(
                'Invalid Region ID',
                Constant::INVALID_ARGUMENT
            );
        }

        return $sysId;
    }

    /**
     * @param $version
     * @return string
     * @throws ClientException
     */
    public static function version($version)
    {
        if (!is_string($version)) {
            throw new ClientException(
                'version must be a string',
                Constant::INVALID_ARGUMENT
            );
        }

        if ($version === '') {
            throw new ClientException(
                'version cannot be empty',
                Constant::INVALID_ARGUMENT
            );
        }

        return $version;
    }

    /**
     * @param $service
     * @return string
     * @throws ClientException
     */
    public static function service($service)
    {
        if (!is_string($service)) {
            throw new ClientException(
                'service must be a string',
                Constant::INVALID_ARGUMENT
            );
        }

        if ($service === '') {
            throw new ClientException(
                'service cannot be empty',
                Constant::INVALID_ARGUMENT
            );
        }

        return $service;
    }

    /**
     * @param $method
     * @return string
     * @throws ClientException
     */
    public static function method($method)
    {
        if (!is_string($method)) {
            throw new ClientException(
                'method must be a string',
                Constant::INVALID_ARGUMENT
            );
        }

        if ($method === '') {
            throw new ClientException(
                'method cannot be empty',
                Constant::INVALID_ARGUMENT
            );
        }

        return $method;
    }

    /**
     * @param $serverUrl
     * @return string
     * @throws ClientException
     */
    public static function serverUrl($serverUrl)
    {
        if (!is_string($serverUrl)) {
            throw new ClientException(
                'serverUrl must be a string',
                Constant::INVALID_ARGUMENT
            );
        }

        if ($serverUrl === '') {
            throw new ClientException(
                'serverUrl cannot be empty',
                Constant::INVALID_ARGUMENT
            );
        }

        return $serverUrl;
    }

    /**
     * @param $path
     * @return string
     * @throws ClientException
     */
    public static function path($path)
    {
        if (!is_string($path)) {
            throw new ClientException(
                'path must be a string',
                Constant::INVALID_ARGUMENT
            );
        }

        if ($path === '') {
            throw new ClientException(
                'path cannot be empty',
                Constant::INVALID_ARGUMENT
            );
        }

        return $path;
    }

    /**
     * @param $pwd
     * @return string
     * @throws ClientException
     */
    public static function pwd($pwd)
    {
        if (!is_string($pwd)) {
            throw new ClientException(
                'pwd must be a string',
                Constant::INVALID_ARGUMENT
            );
        }

        if ($pwd === '') {
            throw new ClientException(
                'pwd cannot be empty',
                Constant::INVALID_ARGUMENT
            );
        }

        return $pwd;
    }

    /**
     * @param $priKey
     * @return string
     * @throws ClientException
     */
    public static function priKey($priKey)
    {
        if (!is_string($priKey)) {
            throw new ClientException(
                'priKey must be a string',
                Constant::INVALID_ARGUMENT
            );
        }

        if ($priKey === '') {
            throw new ClientException(
                'priKey cannot be empty',
                Constant::INVALID_ARGUMENT
            );
        }

        return $priKey;
    }

    /**
     * @throws ClientException
     */
    public static function isPathFile()
    {
        die('错误');
        throw new ClientException(
            'path cannot be empty',
            Constant::INVALID_ARGUMENT
        );
    }

}
