<?php

namespace Tongshangyun\Client\Request;

use Tongshangyun\Client\Traits\AcsTrait;
use Tongshangyun\Client\Traits\HttpTrait;
use Tongshangyun\Client\Traits\RequestTrait;
use Tongshangyun\Client\Support\Sign;
use Tongshangyun\Client\Log\Log;

class Request
{
    use AcsTrait;
    use HttpTrait;
    use RequestTrait;

    /**
     * @var string
     */
    public $logFile;

    public function __construct(){
        $config = require_once __DIR__ . '/../config/config.php';
        $this->sysId($config['sysId']);
        $this->version($config['version']);
        $this->serverUrl($config['serverUrl']);
        $this->path($config['path']);
        $this->pwd($config['pwd']);
        $this->timestamp = date("Y-m-d H:i:s", time());
        $this->logFile = $config['logFile'];
    }

    /**
     *
     */
    public function resolveOption()
    {
        $this->reqClear();
        $this->sign = Sign::string($this->sysId,$this->strReq,$this->timestamp,$this->path,$this->pwd);
        $this->optionsClear();
    }

    /**
     * @return mixed
     */
    private function response()
    {
        Log::logMessage("[请求参数]",json_encode($this->options,JSON_UNESCAPED_UNICODE),Log::INFO,$this->logFile);
        $result = $this->curl($this->serverUrl,$this->optionsSB);
        Log::logMessage("[响应参数]",$result,Log::INFO,$this->logFile);
        return $result;
    }

    /**
     * @param $serverUrl
     * @param $sb
     * @return mixed
     */
    private function curl($serverUrl, $sb)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $serverUrl);
        $reqbody = array();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sb);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-length', count($reqbody)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    public function request()
    {
        $this->resolveOption();
        $result = $this->response();
        $result = json_decode($result,true);
        return $result;
    }
}