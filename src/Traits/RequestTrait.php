<?php


namespace Tongshangyun\Client\Traits;


use Tongshangyun\Client\Filter\ClientFilter;

trait RequestTrait
{
    /**
     * @var string
     */
    public $serverUrl;

    /**
     * @var string
     */
    public $path;

    /**
     * @var string
     */
    public $pwd;

    /**
     * @param $serverUrl
     * @return $this
     * @throws \tongshangyun\client\Exception\ClientException
     */
    public function serverUrl($serverUrl)
    {
        $this->serverUrl = ClientFilter::serverUrl($serverUrl);

        return $this;
    }

    /**
     * @param $path
     * @return $this
     * @throws \tongshangyun\client\Exception\ClientException
     */
    public function path($path)
    {
        $this->path = ClientFilter::path($path);

        return $this;
    }

    /**
     * @param $pwd
     * @return $this
     * @throws \tongshangyun\client\Exception\ClientException
     */
    public function pwd($pwd)
    {
        $this->pwd = ClientFilter::pwd($pwd);

        return $this;
    }

}