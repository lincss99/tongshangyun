<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit61bb75d83adebe0aeca1c85ab7da9620
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Tongshangyun\\Client\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Tongshangyun\\Client\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit61bb75d83adebe0aeca1c85ab7da9620::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit61bb75d83adebe0aeca1c85ab7da9620::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit61bb75d83adebe0aeca1c85ab7da9620::$classMap;

        }, null, ClassLoader::class);
    }
}
