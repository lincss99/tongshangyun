<?php

namespace Tongshangyun\Client\Traits;

use Tongshangyun\Client\Support\Arrays;

trait HttpTrait
{
    /**
     * @var array
     */
    public $options = [];
    /**
     * @var string
     */
    public $optionsSB;

    /**
     * @var array
     */
    public $req = [];

    /**
     * @var string
     */
    public $strReq;

    /**
     * @var array
     */
    public $param = [];

    /**
     * @param array $param
     * @return $this
     */
    public function param(array $param)
    {
        if ($param !== []) {
            $this->param = Arrays::merge([$this->param, $param]);
        }

        return $this;
    }

    public function reqClear(){
        $this->req = [
            'service' => $this->service,
            'method' => $this->method,
            'param' => $this->param,
        ];
        $strReq = json_encode($this->req,JSON_UNESCAPED_UNICODE);
        $this->strReq = str_replace("\r\n", "", $strReq);
    }

    public function optionsClear(){
        $this->options['sysid'] = $this->sysId;
        $this->options['sign'] = $this->sign;
        $this->options['timestamp'] = $this->timestamp;
        $this->options['v'] = $this->version;
        $this->options['req'] = $this->strReq;
        foreach ($this->options as $key => $value) {
            $this->optionsSB .= $key . '=' . urlencode($value) . '&';
        }
    }

}