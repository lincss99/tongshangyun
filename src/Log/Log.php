<?php

namespace Tongshangyun\Client\Log;

class Log
{
    private static $logFile;
    private static $logLevel;
    const DEBUG = 100;
    const INFO  = 75;
    const NOTICE = 50;
    const WARNING =25;
    const ERROR  = 10;
    const CRITICAL = 5;

    /**
     * @param $msg
     * @param int $logLevel
     * @param null $module
     */
    public static function logMessage($msg,$module = null,$logLevel = Log::INFO,$logFile = ''){
        date_default_timezone_set('Asia/shanghai');
        self::$logFile = @fopen($logFile.DIRECTORY_SEPARATOR.date('YmdH').'.log','a+');
        $time = strftime('%x %X',time());
        $msg = str_replace("\t",'',$msg);
        $msg = str_replace("\n",'',$msg);
        $strLogLevel = self::levelToString($logLevel);
        if(isset($module)){
            $module = str_replace(array("\n","\t"),array("",""),$module);
        }
        $logLine = "$time\t$msg\t$strLogLevel\t$module\r\n";
        fwrite(self::$logFile,$logLine);
    }

    /**
     * @param $logLevel
     * @return string
     */
    public static function levelToString($logLevel){
        $ret = '[unknow]';
        switch ($logLevel){
            case LOG::DEBUG:
                $ret = 'LOG::DEBUG';
                break;
            case LOG::INFO:
                $ret = 'LOG::INFO';
                break;
            case LOG::NOTICE:
                $ret = 'LOG::NOTICE';
                break;
            case LOG::WARNING:
                $ret = 'LOG::WARNING';
                break;
            case LOG::ERROR:
                $ret = 'LOG::ERROR';
                break;
            case LOG::CRITICAL:
                $ret = 'LOG::CRITICAL';
                break;
        }
        return $ret;
    }
}